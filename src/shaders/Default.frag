#include <FAKE_INCLUDE/glsl_fake.hxx>
//$END_FAKE$
#version 150
in vec2 TexCoord;
uniform sampler2D Tex1;
out vec4 FragColor;

void main() {
    if (TexCoord.x < 0.5 && TexCoord.y >= 0.5) {
        TexCoord.x += 0.5;
        TexCoord.y -= 0.5;
    } else if (TexCoord.x >= 0.5 && TexCoord.y >= 0.5) {
        TexCoord.x -= 0.5;
        TexCoord.y -= 0.5;
    } else if (TexCoord.x < 0.5 && TexCoord.y < 0.5) {
        TexCoord.x += 0.5;
        TexCoord.y += 0.5;
    } else { //  (TexCoord.x >= 0.5 && TexCoord.y < 0.5)
        TexCoord.x -= 0.5;
        TexCoord.y += 0.5;
    }
    vec4 color = texture(Tex1, TexCoord);

    FragColor = color;
}

