#include <FAKE_INCLUDE/glsl_fake.hxx>
//$END_FAKE$
//-- Vertex
#version 150
in vec3 VertexPosition;
in vec2 VertexTexCoord;
out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}
//-- Fragment
#version 150
in vec2 TexCoord;
uniform sampler2D Tex1;
uniform bool rearrangeQuadrants = false;

out vec4 FragColor;

uniform float contrast;

void main() {
    vec2 v = TexCoord;

    if (rearrangeQuadrants) {
        if (TexCoord.x < 0.5 && TexCoord.y >= 0.5) {
            v.x += 0.5;
            v.y -= 0.5;
        } else if (TexCoord.x >= 0.5 && TexCoord.y >= 0.5) {
            v.x -= 0.5;
            v.y -= 0.5;
        } else if (TexCoord.x < 0.5 && TexCoord.y < 0.5) {
            v.x += 0.5;
            v.y += 0.5;
        } else {
            v.x -= 0.5;
            v.y += 0.5;
        }
    }
    vec4 color = texture(Tex1, v);

    if (contrast > 0.0) {
        color.rgb = (color.rgb - 0.5) / (1.0 - contrast) + 0.5;
    } else {
        color.rgb = (color.rgb - 0.5) * (1.0 + contrast) + 0.5;
    }

    FragColor = color;
}

