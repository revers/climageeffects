#include <FAKE_INCLUDE/glsl_fake.hxx>
//$END_FAKE$
/**
 * 
 * Fragment shader from "Shader Toy"  by Inigo Quilez and others 
 * (http://www.iquilezles.org/apps/shadertoy/).
 * 
 */
#version 150

in vec2 TexCoord;

uniform sampler2D Tex1;
uniform float Time = 0.0f;

uniform float AAScale;
uniform int AALevel;
uniform float InvAALevel;

out vec4 FragColor;

vec3 getPixelColor(vec2 p) {
    vec2 uv;

    float a = atan(p.y, p.x);
    float r = sqrt(dot(p, p));

    uv.x = 7.0 * a / 3.1416;
    uv.y = -Time + sin(7.0 * r + Time) + .7 * cos(Time + 7.0 * a);

    float w = .5 + .5 * (sin(Time + 7.0 * r) + .7 * cos(Time + 7.0 * a));

    vec3 col = texture(Tex1, uv * .5).xyz;

    return col * w;
}

void main() {
    vec2 p = -1.0 + 2.0 * TexCoord;

    if (AALevel == 1) {
        FragColor = vec4(getPixelColor(p), 1.0f);
    } else {
        vec3 color = vec3(0.0, 0.0, 0.0);

        for (int x = 0; x < AALevel; x++) {
            for (int y = 0; y < AALevel; y++) {
                color += getPixelColor(p + vec2(x, y) * InvAALevel * AAScale);
            }
        }

        FragColor = vec4(color * InvAALevel * InvAALevel, 1.0f);
    }
}

