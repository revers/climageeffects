#ifdef FAKE_INCLUDE
#    include <FAKE_INCLUDE/opencl_fake.hxx>
#endif

#define DIVIDE(a, b) native_divide(a, b)
#define SQRT(x) native_sqrt(x)
#define DOT(a, b) dot(a, b)

__constant sampler_t IMAGE_SAMPLER2
= CLK_NORMALIZED_COORDS_FALSE
        | CLK_ADDRESS_CLAMP_TO_EDGE
        | CLK_FILTER_NEAREST;

__kernel void copyTileToBuffers(
        __read_only image2d_t image,
        __global float2* redBuffer,
        __global float2* greenBuffer,
        __global float2* blueBuffer,
        int tileWidth,
        int tileStartX,
        int tileStartY,
        int imageWidth,
        int imageHeight,
        float bloomThreshold,
        float bloom) {

    int x = get_global_id(0);
    int y = get_global_id(1);

    int coordX = tileStartX + x;
    int coordY = tileStartY + y;

    int index = y * tileWidth + x;

    // clamp pixels:
    //coordX = coordX < 0 ? 0 : coordX >= imageWidth ? imageWidth - 1 : coordX;
    //coordY = coordY < 0 ? 0 : coordY >= imageHeight ? imageHeight - 1 : coordY;

    float4 color = read_imagef(image, IMAGE_SAMPLER2, (int2) (coordX, coordY));

    float2 red = (float2) (color.x, 0.0);
    float2 green = (float2) (color.y, 0.0);
    float2 blue = (float2) (color.z, 0.0);

    // Bloom...
    if (red.x > bloomThreshold) {
        red.x *= bloom;
    }
    if (green.x > bloomThreshold) {
        green.x *= bloom;
    }
    if (blue.x > bloomThreshold) {
        blue.x *= bloom;
    }

    redBuffer[index] = red;
    greenBuffer[index] = green;
    blueBuffer[index] = blue;
}

inline float2 cMul(float2 a, float2 b) {
    return (float2) (a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

__kernel void multiplyTiles(
        __global float2* redBuffer,
        __global float2* greenBuffer,
        __global float2* blueBuffer,
        __global float2* maskBuffer,
        int tileWidth,
        float scaleFactor) {

    int x = get_global_id(0);
    int y = get_global_id(1);

    int index = y * tileWidth + x;

    redBuffer[index] = cMul(redBuffer[index], maskBuffer[index]) * scaleFactor;
    greenBuffer[index] = cMul(greenBuffer[index], maskBuffer[index]) * scaleFactor;
    blueBuffer[index] = cMul(blueBuffer[index], maskBuffer[index]) * scaleFactor;
}

__kernel void copyBuffersToImage(
        __write_only image2d_t image,
        __global float2* redBuffer,
        __global float2* greenBuffer,
        __global float2* blueBuffer,
        int tileWidth,
        int tileHeight,
        int tileStartX,
        int tileStartY,
        int imageWidth,
        int imageHeight,
        int iFilterRadius) {

    int x = get_global_id(0) + iFilterRadius;
    int y = get_global_id(1) + iFilterRadius;

    int coordX = tileStartX + x;
    int coordY = tileStartY + y;

    if (coordX < 0 || coordX >= imageWidth
            || coordY < 0 || coordY >= imageHeight) {
        return;
    }

    // Quadrant rempaing:
    //---------------------------------------------------------
    int halfTileWidth = tileWidth >> 1;
    int halfTileHeight = tileHeight >> 1;

    if (x < halfTileWidth && y >= halfTileHeight) {
        x += halfTileWidth;
        y -= halfTileHeight;
    } else if (x >= halfTileWidth && y >= halfTileHeight) {
        x -= halfTileWidth;
        y -= halfTileHeight;
    } else if (x < halfTileWidth && y < halfTileHeight) {
        x += halfTileWidth;
        y += halfTileHeight;
    } else {
        x -= halfTileWidth;
        y += halfTileHeight;
    }
    //==========================================================

    int index = y * tileWidth + x;

    float2 red = redBuffer[index];
    float2 green = greenBuffer[index];
    float2 blue = blueBuffer[index];

    float4 color = (float4) (red.x, green.x, blue.x, 1.0f);

    write_imagef(image, (int2) (coordX, coordY), color);
}
