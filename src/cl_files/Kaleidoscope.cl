#ifdef FAKE_INCLUDE
#    include <FAKE_INCLUDE/opencl_fake.hxx>
#endif

__constant sampler_t IMAGE_SAMPLER
        = CLK_NORMALIZED_COORDS_TRUE
        | CLK_ADDRESS_REPEAT
        | CLK_FILTER_LINEAR;

#define DIVIDE(a, b) native_divide(a, b)
#define SQRT(x) native_sqrt(x)
#define DOT(a, b) dot(a, b)

float4 getPixelColor(__read_only image2d_t input, float2 p, float time) {
    float2 uv;

    float a = atan2(p.y, p.x);
    float r = sqrt(dot(p, p));

    uv.x = 7.0 * a / 3.1416;
    uv.y = -time + sin(7.0 * r + time) + .7 * cos(time + 7.0 * a);

    float w = .5 + .5 * (sin(time + 7.0 * r) + .7 * cos(time + 7.0 * a));

    return read_imagef(input, IMAGE_SAMPLER, uv * 0.5) * w;
}

__kernel void effect(
        __read_only image2d_t input,
        __write_only image2d_t output,
        uint outputWidth,
        uint outputHeight) {

    uint x = get_global_id(0);
    uint y = get_global_id(1);

    if (x >= outputWidth || y >= outputHeight) {
        return;
    }

    float2 texCoord = (float2) (
            DIVIDE((float) x, (float) outputWidth),
            DIVIDE((float) y, (float) outputHeight));
    //--------------------------------------------------------
    //float2 p = -1.0 + 2.0 * texCoord;

//    float4 color = (float4) (0.0, 0.0, 0.0, 0.0);
//
//    float invAALevel = DIVIDE(1.0f, (float) aaLevel);
//
//    for (int x = 0; x < aaLevel; x++) {
//        for (int y = 0; y < aaLevel; y++) {
//
//            color += getPixelColor(input,
//                    p + (float2) (x, y) * invAALevel * aaScale,
//                    time);
//        }
//    }
//
//    color *= invAALevel * invAALevel;
//    color.w = 1.0;

    float4 color = read_imagef(input, IMAGE_SAMPLER, texCoord);//) * w;//getPixelColor(input, p, time);
    //========================================================


    write_imagef(output, (int2) (x, y), color);
}
