/*
 * ImageTransform.cl
 *
 *  Created on: 20-09-2012
 *      Author: Revers
 */

#ifdef FAKE_INCLUDE
#include <FAKE_INCLUDE/opencl_fake.hxx>
#endif

#define DIVIDE(a, b) native_divide(a, b)
#define SQRT(x) native_sqrt(x)
#define DOT(a, b) dot(a, b)

__constant sampler_t IMAGE_SAMPLER = CLK_NORMALIZED_COORDS_TRUE
        | CLK_ADDRESS_MIRRORED_REPEAT
        | CLK_FILTER_NEAREST;

__constant sampler_t IMAGE_SAMPLER2 = CLK_NORMALIZED_COORDS_FALSE
        | CLK_ADDRESS_CLAMP | CLK_FILTER_NEAREST;

//float luma( vec3 color ) {
//return 0.2126 * color.r + 0.7152 * color.g +
//0.0722 * color.b;
//}

__kernel void copyImageLumaToBuffer( __read_only image2d_t image,
        __global float* buffer, uint imageWidth, uint imageHeight,
        uint bufferWidth) {

    uint x = get_global_id(0);
    uint y = get_global_id(1);

    uint indexEven = y * bufferWidth + (2 * x);
    uint indexOdd = y * bufferWidth + (2 * x + 1);

//    float2 texCoord = (float2) (
//                DIVIDE((float) x, (float) imageWidth),
//                DIVIDE((float) y, (float) imageHeight));
//
//        float4 color = read_imagef(image, IMAGE_SAMPLER, texCoord);
//        buffer[indexEven] = 0.2126 * color.x + 0.7152 * color.y + 0.0722 * color.z;

    if (x >= imageWidth || y >= imageHeight) {
        buffer[indexEven] = 0.0f;
    } else {
        float4 color = read_imagef(image, IMAGE_SAMPLER2, (int2) (x, y));
        buffer[indexEven] = 0.2126 * color.x + 0.7152 * color.y + 0.0722 * color.z;
    }

    buffer[indexOdd] = 0.0f;
}

__kernel void copyImageRedToBuffer( __read_only image2d_t image,
        __global float* redBuffer, uint imageWidth, uint imageHeight,
        uint bufferWidth) {

    uint x = get_global_id(0);
    uint y = get_global_id(1);

    uint indexEven = y * bufferWidth + (2 * x);
    uint indexOdd = y * bufferWidth + (2 * x + 1);

    if (x >= imageWidth || y >= imageHeight) {
        redBuffer[indexEven] = 0.0f;
    } else {
        float4 color = read_imagef(image, IMAGE_SAMPLER2, (int2) (x, y));
        redBuffer[indexEven] = color.x;
    }

    redBuffer[indexOdd] = 0.0f;
}

__kernel void copyImageGreenToBuffer( __read_only image2d_t image,
        __global float* greenBuffer, uint imageWidth, uint imageHeight,
        uint bufferWidth) {

    uint x = get_global_id(0);
    uint y = get_global_id(1);

    uint indexEven = y * bufferWidth + (2 * x);
    uint indexOdd = y * bufferWidth + (2 * x + 1);

    if (x >= imageWidth || y >= imageHeight) {
        greenBuffer[indexEven] = 0.0f;
    } else {
        float4 color = read_imagef(image, IMAGE_SAMPLER2, (int2) (x, y));
        greenBuffer[indexEven] = color.y;
    }

    greenBuffer[indexOdd] = 0.0f;
}

__kernel void copyImageBlueToBuffer( __read_only image2d_t image,
        __global float* blueBuffer, uint imageWidth, uint imageHeight,
        uint bufferWidth) {

    uint x = get_global_id(0);
    uint y = get_global_id(1);

    uint indexEven = y * bufferWidth + (2 * x);
    uint indexOdd = y * bufferWidth + (2 * x + 1);

    if (x >= imageWidth || y >= imageHeight) {
        blueBuffer[indexEven] = 0.0f;
    } else {
        float4 color = read_imagef(image, IMAGE_SAMPLER2, (int2) (x, y));
        blueBuffer[indexEven] = color.z;
    }

    blueBuffer[indexOdd] = 0.0f;
}

__inline float complexModule(float re, float im) {
    return SQRT(re * re + im * im);
}

__kernel void copyModBufferToImage( __write_only image2d_t image,
        __global float* buffer, uint imageWidth, uint imageHeight,
        uint bufferWidth) {

    uint x = get_global_id(0);
    uint y = get_global_id(1);

    if (x >= imageWidth || y >= imageHeight) {
        return;
    }

    uint indexEven = y * bufferWidth + (2 * x);
    uint indexOdd = y * bufferWidth + (2 * x + 1);

    float module = complexModule(buffer[indexEven], buffer[indexOdd]);

    if (module > 1.0f) {
        module = 1.0f;
    }
    float4 color = (float4) (module, module, module, 1.0f);

    write_imagef(image, (int2) (x, y), color);
}

__kernel void copyRealPartBufferToImage( __write_only image2d_t image,
        __global float* buffer, uint imageWidth, uint imageHeight,
        uint bufferWidth) {

    uint x = get_global_id(0);
    uint y = get_global_id(1);

    if (x >= imageWidth || y >= imageHeight) {
        return;
    }

    uint indexEven = y * bufferWidth + (2 * x);

    float value = buffer[indexEven];

    if (value > 1.0f) {
        value = 1.0f;
    }
    float4 color = (float4) (value, value, value, 1.0f);

    write_imagef(image, (int2) (x, y), color);
}

__kernel void copyImagPartBufferToImage( __write_only image2d_t image,
        __global float* buffer, uint imageWidth, uint imageHeight,
        uint bufferWidth) {

    uint x = get_global_id(0);
    uint y = get_global_id(1);

    if (x >= imageWidth || y >= imageHeight) {
        return;
    }

    uint indexOdd = y * bufferWidth + (2 * x + 1);

    float value = buffer[indexOdd];

    if (value > 1.0f) {
        value = 1.0f;
    }
    float4 color = (float4) (value, value, value, 1.0f);

    write_imagef(image, (int2) (x, y), color);
}

__kernel void copyPhaseAngleBufferToImage( __write_only image2d_t image,
        __global float* buffer, uint imageWidth, uint imageHeight,
        uint bufferWidth) {

    uint x = get_global_id(0);
    uint y = get_global_id(1);

    if (x >= imageWidth || y >= imageHeight) {
        return;
    }

    uint indexEven = y * bufferWidth + (2 * x);
    uint indexOdd = y * bufferWidth + (2 * x + 1);

    float value = atan2(buffer[indexEven], buffer[indexOdd]);

    value = clamp(value, 0.0f, 1.0f);
    float4 color = (float4) (value, value, value, 1.0f);

    write_imagef(image, (int2) (x, y), color);
}
