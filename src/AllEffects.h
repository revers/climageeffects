/* 
 * File:   AllEffects.h
 * Author: Revers
 *
 * Created on 29 maj 2012, 08:00
 */

#ifndef ALLFILTERS_H
#define	ALLFILTERS_H

#include "Effects/AbstractEffect.h"
//#include "Effects/KaleidoscopeEffect.h"
#include "Effects/FFT2DEffect.h"
#include "Effects/FFT2DLensBlurEffect.h"

#endif	/* ALLFILTERS_H */

