/* 
 * File:   ControlPanel.cpp
 * Author: Revers
 * 
 * Created on 26 kwiecień 2012, 20:33
 */
#include <GL/glew.h>

#include <iostream>

#include <boost/algorithm/string.hpp>

#include "ControlPanel.h"
#include "LoggingDefines.h"
#include "AllEffects.h"
#include "RenderTargetDefines.h"
#include <rev/cl/RevCLContext.h>
#include <GL/gl.h>

#include <rev/cl/RevCLAssert.h>
#include <rev/common/RevStringUtil.h>

using namespace rev;
using namespace log4cplus;
using namespace boost::filesystem;
using namespace std;

#define X_MIN -0.4f
#define X_MAX 1.0f
#define Y_MIN -1.0f
#define Y_MAX 1.0f
#define EFFECT_TOOLBAR_WIDTH 340
#define EFFECT_TOOLBAR_HEIGHT 365
#define CONTROL_TOOLBAR_WIDTH EFFECT_TOOLBAR_WIDTH
#define CONTROL_TOOLBAR_HEIGHT 220
#define CONTROL_TOOLBAR_POS_X 10
#define CONTROL_TOOLBAR_POS_Y 5
#define EFFECT_TOOLBAR_POS_X CONTROL_TOOLBAR_POS_X
#define EFFECT_TOOLBAR_POS_Y 233

Logger ControlPanel::logger = Logger::getInstance("ControlPanel");

ControlPanel::ControlPanel() {
    inputTexture.getTexture().setInitialSampler(GL_LINEAR, GL_LINEAR, GL_REPEAT,
            GL_REPEAT);

    inputTexture.setInitialInternalFormat(GL_RGBA8);

    lmbPressed = false;
    currentEffect = NULL;
    currentEffectStr = NULL;
    selectedBackgroundIndex = 0;
    effectBar = NULL;
    mouseOver = false;
    widthOffset = 0;
    lastMouseWheelPos = 0;
}

ControlPanel::~ControlPanel() {
    for (EffectMap::iterator it = effectMap.begin(); it != effectMap.end(); ++it) {
        delete it->second;
    }
    TwTerminate();
}

bool ControlPanel::init() {

    if (!TwInit(TW_OPENGL_CORE, NULL)) {
        LOG_ERROR(logger, TwGetLastError());
        return false;
    }

    if (!compileGLSLProgram()) {
        return false;
    }

    inputTexture.setFlags(CL_MEM_READ_ONLY);
    outputTexture.setFlags(CL_MEM_WRITE_ONLY);

    quadVBO.create(X_MIN, X_MAX, -1.0, 1.0);
    secondQuadVBO.create(-1.0, 1.0, -1.0, 1.0);
//    if (!quadVBO.create(X_MIN, X_MAX, -1.0, 1.0)) {
//        LOG_ERROR(logger, "quadVBO.create(X_MIN, X_MAX)) FAILED!!");
//        return false;
//    }

//    if (!secondQuadVBO.create()) {
//        LOG_ERROR(logger, "secondQuadVBO.create() FAILED!!");
//        return false;
//    }

    TwDefine(" TW_HELP visible=false ");

    controlBar = TwNewBar("Control");
    TwDefine(" Control color='20 20 20' ");
    TwDefine(" Control valueswidth=165 fontSize=3 ");

    effectBar = TwNewBar("Effects");
    TwDefine(" Effects color='20 20 20' ");
    TwDefine(" Effects valueswidth=165 fontSize=3 ");

    if (!loadBackgroundImages()) {
        LOG_ERROR(logger, "ControlPanel::loadBackgroundImages() FAILED!!");
        return false;
    }

    TwCopyCDStringToClientFunc(copyCDStringToClient);
    TwAddVarRO(controlBar, "__currEffect", TW_TYPE_CDSTRING, &currentEffectStr,
            " label='Current Effect:' ");

    TwAddVarRO(controlBar, "__imgDimension", TW_TYPE_CDSTRING, &imageDimensionStr,
            " label='Image Size:' ");

    TwAddVarCB(controlBar, "Contrast", TW_TYPE_FLOAT, setContrastCallback,
            getContrastCallback, this,
            "label='Contrast:' min=-2.0 max=2.0 step=0.01 ");

    if (!addEffects()) {
        LOG_ERROR(logger, "ControlPanel::addEffects() FAILED!!");
        return false;
    }

    struct {
        int x;
        int y;
    } controlPos = { CONTROL_TOOLBAR_POS_X, CONTROL_TOOLBAR_POS_Y };
    TwSetParam(controlBar, NULL, "position", TW_PARAM_INT32, 2, &controlPos);

    struct {
        int x;
        int y;
    } filterPos = { EFFECT_TOOLBAR_POS_X, EFFECT_TOOLBAR_POS_Y };
    TwSetParam(effectBar, NULL, "position", TW_PARAM_INT32, 2, &filterPos);

    struct {
        int x, y;
    } controlSize = { CONTROL_TOOLBAR_WIDTH, CONTROL_TOOLBAR_HEIGHT };
    TwSetParam(controlBar, NULL, "size", TW_PARAM_INT32, 2, &controlSize);

    struct {
        int x, y;
    } filterSize = { EFFECT_TOOLBAR_WIDTH, EFFECT_TOOLBAR_HEIGHT };
    TwSetParam(effectBar, NULL, "size", TW_PARAM_INT32, 2, &filterSize);

    glslProgram.use();
    glslProgram.setUniform("rearrangeQuadrants", rearrangeQuadrants);

    return true;
}

bool ControlPanel::compileGLSLProgram() {
    const char* shaderFile = "shaders/Default.glsl";

    if (!glslProgram.compileShaderGLSLFile(shaderFile)) {
        LOG_ERROR(logger, "Compilation of file " << shaderFile << " FAILED!!");
        return false;
    }

    glBindAttribLocation(glslProgram.getHandle(), 0, "VertexPosition");
    glBindAttribLocation(glslProgram.getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(glslProgram.getHandle(), 0, "FragColor");

    if (!glslProgram.link()) {
        LOG_ERROR(logger, "Linking program FAILED!!");
        return false;
    }

    glslProgram.use();
    glslProgram.setUniform("Tex1", 0);
    glslProgram.unuse();

    return true;
}

cl_int ControlPanel::acquireGLObjects() {
    cl::CommandQueue& queue = CLContext::get().commandQueue();

    cl_int err = queue.enqueueAcquireGLObjects(&clglMemoryVector, NULL, NULL);
    clAssert(err);
    queue.flush();

    return err;
}

cl_int ControlPanel::releaseGLObjects() {
    cl::CommandQueue& queue = CLContext::get().commandQueue();

    cl_int err = queue.enqueueReleaseGLObjects(&clglMemoryVector, NULL, NULL);
    clAssert(err);
    queue.flush();

    return err;
}

void ControlPanel::refreshCLGLVector() {
    clglMemoryVector.clear();
    if (inputTexture) {
        clglMemoryVector.push_back(inputTexture.getBuffer());
    }
    if (outputTexture) {
        clglMemoryVector.push_back(outputTexture.getBuffer());
    }
}

void ControlPanel::mousePosCallback(int x, int y) {
    TwEventMousePosGLFW(x, y);

    if (x < widthOffset) {
        mouseOver = true;
    } else {
        mouseOver = false;
    }

    if (!mouseOver && lmbPressed && x >= widthOffset && x < windowWidth && y >= 0
            && y < windowHeight) {

        currentEffect->mouseDragged(x - widthOffset, imageHeight - y);
    }
}

void ControlPanel::mouseButtonCallback(int id, int state) {
    TwGetParam(effectBar, NULL, "size", TW_PARAM_INT32, 2,
            &settingsBarBounds.width);
    TwGetParam(effectBar, NULL, "position", TW_PARAM_INT32, 2,
            &settingsBarBounds.x);

    if (id == GLFW_MOUSE_BUTTON_LEFT) {
        int x, y;
        glfwGetMousePos(&x, &y);

        if (state == GLFW_PRESS) {

            if (!mouseOver && x >= widthOffset && x < windowWidth && y >= 0
                    && y < windowHeight) {
                currentEffect->mousePressed(x - widthOffset, imageHeight - y);
            }

            lmbPressed = true;
        } else {
            if (lmbPressed) {
                currentEffect->mouseReleased(x - widthOffset, imageHeight - y);
            }

            lmbPressed = false;
            mouseOver = false;

        }
    }

    TwEventMouseButtonGLFW(id, state);
}

void ControlPanel::resizeCallback(int width, int height) {
    this->windowWidth = width;
    this->windowHeight = height;
    this->imageWidth = (int) ((float) width * (X_MAX - X_MIN) / 2.0f);
    this->imageHeight = height;
    this->widthOffset = windowWidth - imageWidth;

    LOG_TRACE(logger,
            "resizeCallback(" << this->imageWidth << ", " << this->imageHeight << ")");

    if (imageWidth == 0 || imageHeight == 0) {
        return;
    }

    if (currentEffect) {
        currentEffect->resize(this->imageWidth, this->imageHeight);
    }

    TwWindowSize(width, height);
}

bool ControlPanel::addEffects() {
    //----------------------------------------------------
    AbstractEffect* effect = new FFT2DLensBlurEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "FFT2DLensBlurEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["fft2DLensBlur"] = effect;
        effect->use();
    }
    //----------------------------------------------------
    effect = new FFT2DEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "FFT2DEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["fft2D"] = effect;
    }
    //----------------------------------------------------

    if (effectMap.empty()) {
        return false;
    }

    return true;
}

void ControlPanel::render() {
    if (!currentEffect || !inputTexture) {
        TwDraw();
        return;
    }

    acquireGLObjects();
    currentEffect->render();
    releaseGLObjects();

    glslProgram.use();
    outputTexture.getTexture().bind();
    quadVBO.render();

    TwDraw();
}

inline std::string pathToString(const boost::filesystem::path& p) {
    const wchar_t* wFilename = p.c_str();
    return StringUtil::fromWideString(wFilename);
}

bool ControlPanel::loadBackgroundImages() {
    if (!getImagePathList("images/", backgroudImagesVector)) {
        return false;
    }

    if (backgroudImagesVector.size() == 0) {
        LOG_ERROR(logger, "There are no image files in 'images' direcotry!!");
        return false;
    }

    int size = backgroudImagesVector.size();

    TwEnumVal* imagesEnum = new TwEnumVal[size];
    string** filenames = new string*[size];
    int i = 0;

    for (vector<path>::iterator iter = backgroudImagesVector.begin();
            iter != backgroudImagesVector.end();) {
        path p = *(iter++);

        imagesEnum[i].Value = i;

        filenames[i] = new string(pathToString(p));
        imagesEnum[i].Label = filenames[i]->c_str();
        i++;
    }

    TwType imagesType = TwDefineEnum("BackgroundType", imagesEnum, size);
    TwAddVarCB(controlBar, "Background", imagesType, setBackgroundCallback,
            getBackgroundCallback, this, "label='Image:' ");

    for (int i = 0; i < size; i++) {
        delete filenames[i];
    }

    delete[] imagesEnum;
    delete[] filenames;

    string filename = pathToString(backgroudImagesVector[0]); //.file_string();

    if (!changeTexture(filename.c_str())) {
        return false;
    }

    return true;
}

bool ControlPanel::getImagePathList(const char* directory, vector<path>& result) {
    path p(directory);

    static string extensions[] = { ".bmp", ".tga", ".png", ".jpg", ".jpeg" };
    static int extensionsSize = sizeof(extensions) / sizeof(string);

    try {
        if (exists(p)) {

            if (is_directory(p)) {

                for (directory_iterator iter = directory_iterator(p);
                        iter != directory_iterator();) {
                    path p1 = *(iter++);

                    if (!is_regular_file(p1)) {
                        continue;
                    }

                    for (int i = 0; i < extensionsSize; i++) {
                        path extPath = p1.extension();
                        const wchar_t* wExt = extPath.c_str();
                        string ext = StringUtil::fromWideString(wExt);
                        const char* ext1 = extensions[i].c_str();
                        const char* ext2 = ext.c_str();
                        if (strcmp(ext1, ext2) == 0) {
                            result.push_back(p1);
                            break;
                        }
                    }
                }
            } else {
                LOG_ERROR(logger, "'" << p << "' is not a directory!!");
                return false;
            }

        } else {
            LOG_ERROR(logger, "'" << p << "' does not exist!!");
            return false;
        }
    } catch (const filesystem_error& ex) {
        LOG_ERROR(logger, ex.what());
        return false;
    }

    return true;
}

void TW_CALL ControlPanel::setBackgroundCallback(const void* value,
        void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*>(clientData);

    controlPanel->selectedBackgroundIndex = *(const int*) value;
    string filename =
            pathToString(
                    controlPanel->backgroudImagesVector[controlPanel->selectedBackgroundIndex]);
    // controlPanel->backgroudImagesVector[controlPanel->selectedBackgroundIndex].file_string();

    controlPanel->changeTexture(filename.c_str());
}

void TW_CALL ControlPanel::getBackgroundCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*>(clientData);

    *(int*) value = controlPanel->selectedBackgroundIndex;
}

void TW_CALL ControlPanel::resetButtonCallback(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*>(clientData);

    LOG_TRACE(logger, "Reset current filter ");

    if (controlPanel->currentEffect != NULL) {
        controlPanel->currentEffect->reset();
    }
}

#define SIZE_IN_MB(x) ((float)(x) / (1024.0f * 1024.f))

bool ControlPanel::changeTexture(const char* textureFile) {

    // texture.setInitialFormat(GL_RGBA, GL_UNSIGNED_BYTE);
    if (!inputTexture.load(textureFile)) {
        LOG_ERROR(logger, "Loading texture '" << textureFile << "' FAILED!");
        return false;
    }

    if (!outputTexture.create(inputTexture.getWidth(), inputTexture.getHeight())) {
        LOG_ERROR(logger, "outputTexture.create() FAILED!!");
        return false;
    }

    std::string dim;
    dim += StringUtil::toString(inputTexture.getWidth()) + "x"
            + StringUtil::toString(inputTexture.getHeight());

    copyCDStringToClient(&imageDimensionStr, dim.c_str());

    refreshCLGLVector();

    LOG_INFO(logger, "Texture '" << textureFile << "' loaded successfully.");

    int totalSize = inputTexture.getSize() + outputTexture.getSize();
    char buff[32];
    sprintf(buff, "%.2f", SIZE_IN_MB(totalSize));
    LOG_INFO(logger, "Input and output texture size: " << buff << " MB.");

    if (currentEffect) {
        currentEffect->textureChanged(inputTexture);
    }

    return true;
}

void TW_CALL ControlPanel::copyCDStringToClient(char** destPtr, const char* src) {
    int srcLen = (src != NULL) ? strlen(src) : 0;
    int destLen = (*destPtr != NULL) ? strlen(*destPtr) : 0;

    if (*destPtr == NULL)
        *destPtr = (char *) malloc(srcLen + 1);
    else if (srcLen > destLen)
        *destPtr = (char *) realloc(*destPtr, srcLen + 1);

    if (srcLen > 0)
        strncpy(*destPtr, src, srcLen);
    (*destPtr)[srcLen] = '\0';
}

void ControlPanel::setEffect(AbstractEffect* effect) {
    this->currentEffect = effect;
    copyCDStringToClient(&currentEffectStr, effect->getName());

    LOG_INFO(logger, "Using effect: " << currentEffect->getName());

    TwRefreshBar(controlBar);

    currentEffect->textureChanged(inputTexture);
}

void TW_CALL
ControlPanel::setContrastCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*>(clientData);

    controlPanel->setContrast(*(const float*) value);
}

void TW_CALL
ControlPanel::getContrastCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*>(clientData);

    *(float*) value = controlPanel->getContrast();
}

void ControlPanel::setRearrangeQuadrants(bool rearrangeQuadrants) {
    this->rearrangeQuadrants = rearrangeQuadrants;
    glslProgram.use();
    glslProgram.setUniform("rearrangeQuadrants", rearrangeQuadrants);
}

