/* 
 * File:   ControlPanel.h
 * Author: Revers
 *
 * Created on 26 kwiecień 2012, 20:33
 */

#ifndef CONTROLPANEL_H
#define	CONTROLPANEL_H

#include <boost/filesystem.hpp>
#include "Effects/AbstractEffect.h"

#include <GL/glfw.h>
#include <AntTweakBar.h>

#include <vector>
#include <map>

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <rev/gl/RevGLTextureRenderTarget.h>
#include <rev/gl/RevGLTexture2D.h>
#include <rev/cl/RevCLTexture2D.h>
#include <rev/gl/RevGLSLProgram.h>

#include "RevVBOQuadVT.h"

typedef std::map<std::string, AbstractEffect*> EffectMap;

class ControlPanel {
    static log4cplus::Logger logger;
    typedef std::vector<boost::filesystem::path> PathVector;

    friend class AbstractEffect;

    struct TweakBarBounds {
        int x;
        int y;
        int width;
        int height;
    };

    TweakBarBounds settingsBarBounds;
    TwBar *effectBar;
    TwBar *controlBar;

    AbstractEffect* currentEffect;

    PathVector backgroudImagesVector;
    int selectedBackgroundIndex;

    char* currentEffectStr = NULL;
    char* imageDimensionStr = NULL;
    bool lmbPressed;

    int lastMouseWheelPos;

    float contrast = 0.0f;
    bool rearrangeQuadrants = true;

    std::vector<cl::Memory> clglMemoryVector;
    rev::GLSLProgram glslProgram;
public:

    EffectMap effectMap;

    rev::CLTexture2D inputTexture;
    rev::CLTexture2D outputTexture;

    rev::VBOQuadVT quadVBO;
    rev::VBOQuadVT secondQuadVBO;

    int imageWidth;
    int imageHeight;
    int windowWidth;
    int windowHeight;
    int widthOffset;
    bool mouseOver;

    ControlPanel();

    virtual ~ControlPanel();

    void render();

    bool init();

    const char* getCurrentEffectName() {
        if (!currentEffect) {
            return NULL;
        }
        return currentEffect->getName();
    }

    void setEffect(AbstractEffect* effect);

    AbstractEffect* getEffect() {
        return currentEffect;
    }

    void keyDownCallback(int key, int action) {
        TwEventKeyGLFW(key, action);
    }

    void resizeCallback(int width, int height);

    void mousePosCallback(int x, int y);

    void mouseButtonCallback(int id, int state);

    void mouseWheelCallback(int pos) {
        int diff = pos - lastMouseWheelPos;
        lastMouseWheelPos = pos;

        if (!mouseOver) {
            currentEffect->mouseWheelCallback(diff);
        }

        refresh();
        TwEventMouseWheelGLFW(pos);
    }

    void refresh() {
        TwRefreshBar(effectBar);
        TwRefreshBar(controlBar);
    }

    cl_int acquireGLObjects();
    cl_int releaseGLObjects();

    float getContrast() const {
        return contrast;
    }

    void setContrast(float contrast) {
        this->contrast = contrast;
        glslProgram.use();
        glslProgram.setUniform("contrast", contrast);
    }

    bool isRearrangeQuadrants() const {
        return rearrangeQuadrants;
    }

    void setRearrangeQuadrants(bool rearrangeQuadrants);

private:
    bool compileGLSLProgram();
    void refreshCLGLVector();

    bool changeTexture(const char* textureFile);

    static void TW_CALL setContrastCallback(const void* value, void* clientData);
    static void TW_CALL getContrastCallback(void* value, void* clientData);

    static void TW_CALL copyCDStringToClient(char** destPtr, const char* src);

    static void TW_CALL setBackgroundCallback(const void* value, void* clientData);
    static void TW_CALL getBackgroundCallback(void* value, void* clientData);

    static void TW_CALL resetButtonCallback(void* clientData);

    bool loadBackgroundImages();

    bool getImagePathList(const char* directory,
            std::vector<boost::filesystem::path>& result);

    bool addEffects();
};

#endif	/* CONTROLPANEL_H */

