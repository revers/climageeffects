/* 
 * File:   FFT2DEffect.h
 * Author: Revers
 *
 * Created on 12 lipiec 2012, 22:31
 */

#ifndef FFTLENSBLUREFFECT_H
#define	FFTLENSBLUREFFECT_H

#include "AbstractEffect.h"

#include <rev/cl/RevCLBuffer.h>
#include "CLImageTransformProgram.h"
#include "FFT2D.h"

namespace log4cplus {
    class Logger;
}

class FFT2DEffect: public AbstractEffect {
    static const int displayModes = 5;

    enum DisplayMode {
        MODE_MODULE, MODE_PHASE_ANGLE, MODE_REAL_PART,
        MODE_IMAGINARY_PART, MODE_ORIG_IMAGE
    };

    DisplayMode displayMode = MODE_MODULE;

    static log4cplus::Logger logger;
    float scaleFactor;
    int interBuffWidth;
    int interBuffHeight;

    rev::CLBuffer colorBuffer;
    rev::CLBuffer interBuffer;

    rev::CLImageTransformProgram imageTransProgram;
    FFT2D fft2D;

    bool rearrangeQuadrants = true;

public:
    FFT2DEffect(ControlPanel* controlPanel);

    virtual ~FFT2DEffect() {
    }

    /**
     * @Override
     */
    bool init();

    /**
     * @Override
     */
    void reset();

    /**
     * @Override
     */
    const char* getName();

    /**
     * @Override
     */
    void textureChanged(rev::CLTexture2D& texture);

    /**
     * @Override
     */
    void render();

    /**
     * @Override
     */
    void use();

private:
    bool initBuffers();

    static void TW_CALL setScaleFactorCallback(const void* value, void* clientData);
    static void TW_CALL getScaleFactorCallback(void* value, void* clientData);

    static void TW_CALL setRearrangeQuadrantsCallback(const void* value,
            void* clientData);
    static void TW_CALL getRearrangeQuadrantsCallback(void* value,
            void* clientData);

    static void TW_CALL setDisplayModeCallback(const void* value, void* clientData);

    static void TW_CALL getDisplayModeCallback(void* value, void* clientData);
};

#endif	/* FFTLENSBLUREFFECT_H */

