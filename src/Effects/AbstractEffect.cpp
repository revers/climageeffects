/* 
 * File:   AbstractEffect.h
 * Author: Revers
 *
 * Created on 16 czerwiec 2012, 21:50
 */

#include <iostream>
#include <vector>
#include <string>
#include <string.h>

#include "../ControlPanel.h"
#include "AbstractEffect.h"
#include <rev/cl/RevCLContext.h>

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <rev/cl/RevCLAssert.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

Logger AbstractEffect::logger = Logger::getInstance("effects.AbstractEffect");

AbstractEffect::AbstractEffect(ControlPanel* controlPanel_) :
        controlPanel(controlPanel_) {
    effectBar = controlPanel->effectBar;

    gridSize[0] = 0;
    gridSize[1] = 0;
    localSize[0] = LOCAL_SIZE_X;
    localSize[1] = LOCAL_SIZE_Y;
}

void AbstractEffect::addLoadButton(const char* twName, const char* twGroup) {
    std::string params = "label='Load Filter' group='";
    params += twGroup;
    params += "' ";
    TwAddButton(effectBar, twName, loadButtonCallback, this, params.c_str());
}

void AbstractEffect::use() {
    controlPanel->setEffect(this);
    updateWidthAndHeight();

    controlPanel->setRearrangeQuadrants(false);
}

void AbstractEffect::resize(int width, int height) {

}

void AbstractEffect::updateWidthAndHeight() {
    int width = controlPanel->outputTexture.getWidth();
    int height = controlPanel->outputTexture.getHeight();

    gridSize[0] = roundUp(LOCAL_SIZE_X, width);
    gridSize[1] = roundUp(LOCAL_SIZE_Y, height);

    if (!effectKernel()) {
        return;
    }
    cl_int err = effectKernel.setArg(EffectKernel::ARG_WIDTH, width);
    clAssert(err);

    err = effectKernel.setArg(EffectKernel::ARG_HEIGHT, height);
    clAssert(err);

    err = effectKernel.setArg(EffectKernel::ARG_OUTPUT,
            controlPanel->outputTexture.getBuffer());
    clAssert(err);

}

void TW_CALL AbstractEffect::loadButtonCallback(void* clientData) {
    AbstractEffect* filter = static_cast<AbstractEffect*>(clientData);
    filter->updateWidthAndHeight();
    filter->use();
}

bool AbstractEffect::amILoaded() {
    return strcmp(getName(), controlPanel->getEffect()->getName()) == 0;
}

void AbstractEffect::render() {
    // glFlush();

    cl_int err = effectKernel.setArg(EffectKernel::ARG_INPUT,
            controlPanel->inputTexture.getBuffer());

    clAssert(err);

    cl::CommandQueue& queue = CLContext::get().commandQueue();
    err = queue.enqueueNDRangeKernel(effectKernel, cl::NullRange,
            cl::NDRange(gridSize[0], gridSize[1]),
            cl::NDRange(localSize[0], localSize[1]), NULL, NULL);

    clAssert(err);

      //  clCommandQueue.flush();
}

bool AbstractEffect::createProgram(const char* kernelFile,
        cl::Program& outProgram) {

    if (CLContext::get().isInitialized() == false) {
        LOG_ERROR(logger, "OpenCL context not initialized!!");
        return false;
    }

    cl_int err;

    string sourceCode;
    std::ifstream file(kernelFile);

    if (!file) {
        LOG_ERROR(logger, "Failed to load file (dosn't exist?): " << kernelFile);
        return false;
    }

    sourceCode = std::string(std::istreambuf_iterator<char>(file),
            (std::istreambuf_iterator<char>()));

    LOG_INFO(logger, "Compiling file '" << kernelFile << "'...");

    cl::Program::Sources sources(1,
            std::make_pair(sourceCode.c_str(), sourceCode.length()));

    outProgram = cl::Program(CLContext::get().context(), sources, &err);
    if (err != CL_SUCCESS) {
        LOG_ERROR(logger, "Failed to create a CL program!");
        return false;
    }

    string flags = "-cl-fast-relaxed-math";

    vector<cl::Device> devices;
    devices.push_back(CLContext::get().device());

    err = outProgram.build(devices, flags.c_str());

    if (err != CL_SUCCESS) {

        string str = outProgram.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices[0]);
        LOG_ERROR(logger, "Program build error: " << str);
        return false;
    }

    return true;
}

void AbstractEffect::textureChanged(rev::CLTexture2D& texture) {
    updateWidthAndHeight();
}
