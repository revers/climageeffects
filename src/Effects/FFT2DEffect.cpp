/* 
 * File:   FFT2DEffect.cpp
 * Author: Revers
 * 
 * Created on 12 lipiec 2012, 22:31
 */

#include "../ControlPanel.h"

#include <cmath>
#include <GL/glew.h>
#include <GL/gl.h>

#include "FFT2DEffect.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <rev/common/RevAssert.h>
#include <rev/common/RevProfiler.h>
#include <rev/cl/RevCLAssert.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define EFFECT_GROUP "FFT 2D"
#define EFFECT_PREFIX "fft2D_"

#define DEFAULT_SCALE_FACTOR 80.0f

Logger FFT2DEffect::logger = Logger::getInstance("effects.FFT2D");

FFT2DEffect::FFT2DEffect(ControlPanel* controlPanel) :
        AbstractEffect(controlPanel) {
    scaleFactor = DEFAULT_SCALE_FACTOR;

    interBuffWidth = 0;
    interBuffHeight = 0;
}


void FFT2DEffect::use() {
    AbstractEffect::use();
    controlPanel->setRearrangeQuadrants(rearrangeQuadrants);
}

bool FFT2DEffect::init() {
    LOG_TRACE(logger, "init()");

    if (!initBuffers()) {
        LOG_ERROR(logger, "initBuffers() FAILED!!");
        return false;
    }

    if (!fft2D.init(16, 16)) {
        LOG_ERROR(logger, "ff2D.init() FAILED!!");
        return false;
    }

    if (!imageTransProgram.init()) {
        LOG_ERROR(logger, "imageTransProgram.init() FAILED!!");
        return false;
    }

    reset();

    TwEnumVal displayEV[displayModes] = { { MODE_MODULE, "Module" }, {
            MODE_PHASE_ANGLE, "Phase Angle" }, { MODE_REAL_PART, "Real Part" }, {
            MODE_IMAGINARY_PART, "Imaginary Part" }, { MODE_ORIG_IMAGE,
            "Original Image" } };
    TwType shapeType = TwDefineEnum("DisplayType", displayEV, displayModes);
    TwAddVarCB(effectBar, "DisplayMode", shapeType, setDisplayModeCallback,
            getDisplayModeCallback, this, "label='Display Mode' group='"
            EFFECT_GROUP "' ");

    TwAddVarCB(effectBar, EFFECT_PREFIX "ScaleFactor", TW_TYPE_FLOAT,
            setScaleFactorCallback, getScaleFactorCallback, this,
            "label='Scale Factor:' min=0.1 max=400.0 step=0.1 group='"
            EFFECT_GROUP "' ");

    TwAddVarCB(effectBar, EFFECT_PREFIX "RearrangeQuadrants", TW_TYPE_BOOLCPP,
            setRearrangeQuadrantsCallback, getRearrangeQuadrantsCallback, this,
            "label='Rearrange Quadrants:' group='" EFFECT_GROUP "' ");

    addLoadButton(EFFECT_PREFIX "effect", EFFECT_GROUP);

    int opened = 1;
    TwSetParam(effectBar, EFFECT_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

#define SIZE_IN_MB(x) ((float)(x) / (1024.0f * 1024.f))

bool FFT2DEffect::initBuffers() {
    int imageWidth = controlPanel->inputTexture.getWidth();
    int imageHeight = controlPanel->inputTexture.getHeight();

    int width = (int) pow(2.0, ceil(log2((double) imageWidth)));
    int height = (int) pow(2.0, ceil(log2((double) imageHeight)));

    if (interBuffWidth == width && interBuffHeight == height) {
        return true;
    }

    LOG_INFO(logger,
            "Creating new intermediate buffer " << width << "x" << height << " (for image " << imageWidth << "x" << imageHeight << ").");

    if (!colorBuffer.create(width * height * COMPONENT_SIZE)) {
        assert(false);
        return false;
    }

    if (!interBuffer.create(width * height * COMPONENT_SIZE)) {
        assert(false);
        return false;
    }

    interBuffWidth = width;
    interBuffHeight = height;

    return true;
}

void FFT2DEffect::render() {
    cl::CommandQueue& queue = CLContext::get().commandQueue();
    if (displayMode == MODE_ORIG_IMAGE) {

        controlPanel->inputTexture.copyTo(controlPanel->outputTexture);
        queue.finish();
        return;
    }

    imageTransProgram.copyImageColorToBuffer(controlPanel->inputTexture,
            colorBuffer, interBuffWidth, interBuffHeight);
    queue.finish();

    fft2D.fft(colorBuffer, interBuffer, scaleFactor);
    queue.finish();

    if (displayMode == MODE_MODULE) {
        imageTransProgram.modBufferToImage(controlPanel->outputTexture, colorBuffer,
                interBuffWidth, interBuffHeight);
    } else if (displayMode == MODE_PHASE_ANGLE) {
        imageTransProgram.phaseAngleBufferToImage(controlPanel->outputTexture,
                colorBuffer, interBuffWidth, interBuffHeight);
    } else if (displayMode == MODE_REAL_PART) {
        imageTransProgram.realPartBufferToImage(controlPanel->outputTexture,
                colorBuffer, interBuffWidth, interBuffHeight);
    } else if (displayMode == MODE_IMAGINARY_PART) {
        imageTransProgram.imagPartBufferToImage(controlPanel->outputTexture,
                colorBuffer, interBuffWidth, interBuffHeight);
    }
    queue.finish();
}

void FFT2DEffect::textureChanged(rev::CLTexture2D& texture) {
    if (!initBuffers()) {
        LOG_ERROR(logger, "initBuffers() FAILED!!");
    }
    int width = texture.getWidth();
    int height = texture.getHeight();

    int totalSize = interBuffer.getSize() + colorBuffer.getSize();

    char buff[32];
    sprintf(buff, "%.2f", SIZE_IN_MB(totalSize));

    int totalSizeWithTexture = totalSize + controlPanel->inputTexture.getSize()
            + controlPanel->outputTexture.getSize();

    char buff2[32];
    sprintf(buff2, "%.2f", SIZE_IN_MB(totalSizeWithTexture));

    LOG_INFO(logger,
            "Total size of CL buffers: " << buff << " MB" << " (with textures: " << buff2 << " MB).");

    Profiler p;
    p.start();

    if (!fft2D.init(interBuffWidth, interBuffHeight)) {
        LOG_ERROR(logger, "ff2D.init() FAILED!!");
        assert(false);
    }
    p.stop();
    LOG_INFO(logger,
            "fft2D.init() took " << StringUtil::format("%.6f", p.getTotalTimeSec()) << " seconds.\n");

}

void FFT2DEffect::reset() {

    scaleFactor = DEFAULT_SCALE_FACTOR;
}

const char* FFT2DEffect::getName() {

    return EFFECT_GROUP;
}

void TW_CALL
FFT2DEffect::setScaleFactorCallback(const void* value, void* clientData) {
    FFT2DEffect* effect = static_cast<FFT2DEffect*>(clientData);
    if (!effect->amILoaded()) {

        return;
    }

    effect->scaleFactor = *(const float*) value;
}

void TW_CALL
FFT2DEffect::getScaleFactorCallback(void* value, void* clientData) {

    FFT2DEffect* effect = static_cast<FFT2DEffect*>(clientData);

    *(float*) value = effect->scaleFactor;
}

void TW_CALL
FFT2DEffect::setRearrangeQuadrantsCallback(const void* value, void* clientData) {
    FFT2DEffect* effect = static_cast<FFT2DEffect*>(clientData);
    if (!effect->amILoaded()) {
        return;
    }

    effect->rearrangeQuadrants = *(const bool*) value;

    effect->controlPanel->setRearrangeQuadrants(effect->rearrangeQuadrants);
}

void TW_CALL
FFT2DEffect::getRearrangeQuadrantsCallback(void* value, void* clientData) {

    FFT2DEffect* effect = static_cast<FFT2DEffect*>(clientData);

    *(bool*) value = effect->rearrangeQuadrants;
}

void TW_CALL
FFT2DEffect::setDisplayModeCallback(const void* value, void* clientData) {
    FFT2DEffect* effect = static_cast<FFT2DEffect*>(clientData);

    effect->displayMode = *(const DisplayMode*) value;
}

void TW_CALL
FFT2DEffect::getDisplayModeCallback(void* value, void* clientData) {
    FFT2DEffect* effect = static_cast<FFT2DEffect*>(clientData);

    *(DisplayMode*) value = effect->displayMode;
}
