/* 
 * File:   KaleidoscopeEffect.cpp
 * Author: Revers
 * 
 * Created on 18 czerwiec 2012, 07:55
 */

#include "../ControlPanel.h"

#include <GL/glew.h>
#include <GL/gl.h>

#include "KaleidoscopeEffect.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <rev/cl/RevCLAssert.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define EFFECT_GROUP "Kaleidoscope"
#define EFFECT_PREFIX "kalei_"
#define KERNEL_FILE "cl_files/Kaleidoscope.cl"


Logger KaleidoscopeEffect::logger = Logger::getInstance("effects.Kaleidoscope");

KaleidoscopeEffect::KaleidoscopeEffect(ControlPanel* controlPanel)
: AbstractEffect(controlPanel) {
}

bool KaleidoscopeEffect::init() {
    LOG_TRACE(logger, "init()");

    if (!createProgram(KERNEL_FILE, clProgram)) {
        LOG_ERROR(logger, "Compilation of file " KERNEL_FILE " FAILED!!");
        return false;
    }

    cl_int err;
    effectKernel = cl::Kernel(clProgram, "effect", &err);
    if (err != CL_SUCCESS) {
        LOG_ERROR(logger, "creating kernel 'effect' FAILED");
        return false;
    }

    addLoadButton(EFFECT_PREFIX "effect", EFFECT_GROUP);

    int opened = 0;
    TwSetParam(effectBar, EFFECT_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}


void KaleidoscopeEffect::reset() {
    //    if (!program) {
    //        return;
    //    }
    // TODO
}

const char* KaleidoscopeEffect::getName() {
    return EFFECT_GROUP;
}

