/*
 * CLTransposeProgram.h
 *
 *  Created on: 20-09-2012
 *      Author: Revers
 */

#ifndef CLTRANSPOSEPROGRAM_H_
#define CLTRANSPOSEPROGRAM_H_

#include "../CLImageEffectsConfig.h"
#include <rev/cl/RevCLProgram.h>
#include <rev/cl/RevCLBuffer.h>

namespace log4cplus {
    class Logger;
}
;

namespace rev {

    class CLTransposeProgram {
        static log4cplus::Logger logger;

        CLProgram program;
        cl::Kernel mulNormKernel;
    public:
        CLTransposeProgram() {
        }
        virtual ~CLTransposeProgram() {
        }

        bool init();

        void transposeMulNorm(rev::CLBuffer& inBuffer, rev::CLBuffer& outBuffer,
                int width, int height, float scaleFactor);

    private:
        int roundUp(int group_size, int global_size) {
            if (group_size == 0)
                return 0;
            int r = global_size % group_size;
            if (r == 0) {
                return global_size;
            } else {
                return global_size + group_size - r;
            }
        }

    };

}
/* namespace rev */
#endif /* CLTRANSPOSEPROGRAM_H_ */
