/* 
 * File:   AbstractEffect.h
 * Author: Revers
 *
 * Created on 16 czerwiec 2012, 21:50
 */

#ifndef ABSTRACT_EFFECT_H
#define	ABSTRACT_EFFECT_H

#include "../CLImageEffectsConfig.h"
#include "../LoggingDefines.h"

#include <rev/gl/RevGLSLProgram.h>

#include <AntTweakBar.h>
#include <CL/cl.hpp>

#include <rev/cl/RevCLTexture2D.h>
#include <rev/common/RevAssert.h>


class ControlPanel;

namespace EffectKernel {

    enum Arguments {
        ARG_INPUT = 0,
        ARG_OUTPUT,
        ARG_WIDTH,
        ARG_HEIGHT,
        NEXT_ARG,
    };
}

namespace log4cplus {
    class Logger;
};

class AbstractEffect {
    friend class ControlPanel;

    static log4cplus::Logger logger;
protected:
    ControlPanel* controlPanel;
    TwBar* effectBar;
    cl::Program clProgram;
    cl::Kernel effectKernel;
    int gridSize[2];
    int localSize[2];
public:
    AbstractEffect(ControlPanel* controlPanel_);

    virtual ~AbstractEffect() {
    }

    virtual bool init() = 0;
    virtual void reset() = 0;
    virtual const char* getName() = 0;
    virtual void use();
    virtual void render();


    virtual void resize(int width, int height);

    virtual void mousePressed(int x, int y) {
    }

    virtual void mouseReleased(int x, int y) {
    }

    virtual void mouseDragged(int x, int y) {
    }

    virtual void mouseWheelCallback(int diff) {
    }

    virtual void textureChanged(rev::CLTexture2D& texture);

protected:
    bool createProgram(const char* kernelFile, cl::Program& outProgram);

    void addLoadButton(const char* twName, const char* twGroup);

    bool amILoaded();

    int roundUp(int group_size, int global_size) {
        if (group_size == 0) return 0;
        int r = global_size % group_size;
        if (r == 0) {
            return global_size;
        } else {
            return global_size + group_size - r;
        }
    }
private:
    static void TW_CALL loadButtonCallback(void* clientData);

    void updateWidthAndHeight();
};


#endif	/* ABSTRACT_FILTER_H */

