/*
 * FFT2D.h
 *
 *  Created on: 21-09-2012
 *      Author: Revers
 */

#ifndef FFT2D_H_
#define FFT2D_H_

#include <rev/cla/api/RevICLFFTInPlace.h>
#include <rev/cla/api/RevCLFFTInPlaceAbstractFactory.h>
#include <rev/common/RevArray.h>
#include <rev/common/RevArrayUtil.h>

#include "CLTransposeProgram.h"

#define COMPONENT_COUNT 2
#define COMPONENT_SIZE (sizeof(cl_float) * COMPONENT_COUNT)

namespace log4cplus {
    class Logger;
}

class FFT2D {
    static log4cplus::Logger logger;
    rev::CLFFTInPlacePtr clFFTHorizontalPtr;
    rev::CLFFTInPlacePtr clFFTVerticalPtr;

    rev::CLTransposeProgram transposeProgram;

    int width = 0;
    int height = 0;
public:
    FFT2D() {
    }
    virtual ~FFT2D() {
    }

    bool init(int width, int height);

    bool reinit(int width, int height);

    void fft(rev::CLBuffer& redBuffer, rev::CLBuffer& interBuffer, float scaleFactor);

private:
    bool createFFTAlgorithms();
};

#endif /* FFT2D_H_ */
