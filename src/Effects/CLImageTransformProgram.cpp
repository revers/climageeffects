/*
 * CLImageTransformProgram.cpp
 *
 *  Created on: 20-09-2012
 *      Author: Revers
 */

#include "CLImageTransformProgram.h"

#include <string>
#include "CLImageTransformProgram.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include "../LoggingDefines.h"

#include <rev/common/RevAssert.h>
#include <rev/cl/RevCLAssert.h>

using namespace log4cplus;

using namespace std;
using namespace rev;

#define REVCLA_USE_FAST_MATH
#define KERNEL_FILENAME "cl_files/ImageTransform.cl"

Logger CLImageTransformProgram::logger = Logger::getInstance(
        "effects.rev.CLImageTransformProgram");

namespace KernelImageToBuffer {
    enum {
        ARG_IMAGE, ARG_BUFFER, ARG_IMG_WIDTH, ARG_IMG_HEIGHT, ARG_BUFFER_WIDTH
    };
}

namespace KernelBufferToImage {
    enum {
        ARG_IMAGE,
        ARG_BUFFER,
        ARG_IMG_WIDTH,
        ARG_IMG_HEIGHT,
        ARG_BUFFER_WIDTH,
        ARG_SCALE_FACTOR
    };
}

bool CLImageTransformProgram::init() {
#ifdef REVCLA_USE_FAST_MATH
    string programFlags = "-cl-fast-relaxed-math";
#else
    string programFlags = "";
#endif
    program.setFlags(programFlags);

    LOG_TRACE(logger, "Compiling " KERNEL_FILENAME);

    if (!program.compileFromFile(KERNEL_FILENAME)) {
        LOG_ERROR(logger, "Failed to compile " KERNEL_FILENAME);
        return false;
    }

    cl_int err;
    colorToBufferKernel = cl::Kernel(program(), "copyImageLumaToBuffer", &err);
    clAssert(err);

    if (err != CL_SUCCESS) {
        return false;
    }

    modBufferToImgKernel = cl::Kernel(program(), "copyModBufferToImage", &err);
    clAssert(err);

    if (err != CL_SUCCESS) {
        return false;
    }

    phaseAngleBufferToImgKernel = cl::Kernel(program(),
            "copyPhaseAngleBufferToImage", &err);
    clAssert(err);

    if (err != CL_SUCCESS) {
        return false;
    }

    realPartBufferToImgKernel = cl::Kernel(program(), "copyRealPartBufferToImage",
            &err);
    clAssert(err);

    if (err != CL_SUCCESS) {
        return false;
    }

    imagPartBufferToImgKernel = cl::Kernel(program(), "copyImagPartBufferToImage",
            &err);
    clAssert(err);

    if (err != CL_SUCCESS) {
        return false;
    }

    return true;
}

void CLImageTransformProgram::copyImageColorToBuffer(rev::CLTexture2D& img,
        rev::CLBuffer& buffer, int interBuffWidth, int interBuffHeight) {
    cl_int err;
    err = colorToBufferKernel.setArg(KernelImageToBuffer::ARG_IMAGE,
            img.getBuffer());
    clAssert(err);

    err = colorToBufferKernel.setArg(KernelImageToBuffer::ARG_IMG_WIDTH,
            img.getWidth());
    clAssert(err);

    err = colorToBufferKernel.setArg(KernelImageToBuffer::ARG_IMG_HEIGHT,
            img.getHeight());
    clAssert(err);

    err = colorToBufferKernel.setArg(KernelImageToBuffer::ARG_BUFFER,
            buffer.getBuffer());
    clAssert(err);

    err = colorToBufferKernel.setArg(KernelImageToBuffer::ARG_BUFFER_WIDTH,
            interBuffWidth * 2);
    clAssert(err);

    cl::CommandQueue& queue = CLContext::get().commandQueue();
    err = queue.enqueueNDRangeKernel(colorToBufferKernel, cl::NullRange,
            cl::NDRange(interBuffWidth, interBuffHeight),
            cl::NDRange(LOCAL_SIZE_X, LOCAL_SIZE_Y), NULL, NULL);
    clAssert(err);
}

void CLImageTransformProgram::modBufferToImage(rev::CLTexture2D& img,
        rev::CLBuffer& buffer, int interBuffWidth, int interBuffHeight) {
    cl_int err;
    err = modBufferToImgKernel.setArg(KernelBufferToImage::ARG_IMAGE,
            img.getBuffer());
    clAssert(err);

    err = modBufferToImgKernel.setArg(KernelBufferToImage::ARG_IMG_WIDTH,
            img.getWidth());
    clAssert(err);

    err = modBufferToImgKernel.setArg(KernelBufferToImage::ARG_IMG_HEIGHT,
            img.getHeight());
    clAssert(err);

    err = modBufferToImgKernel.setArg(KernelBufferToImage::ARG_BUFFER,
            buffer.getBuffer());
    clAssert(err);

    err = modBufferToImgKernel.setArg(KernelBufferToImage::ARG_BUFFER_WIDTH,
            interBuffWidth * 2);
    clAssert(err);

    cl::CommandQueue& queue = CLContext::get().commandQueue();
    err = queue.enqueueNDRangeKernel(modBufferToImgKernel, cl::NullRange,
            cl::NDRange(interBuffWidth, interBuffHeight),
            cl::NDRange(LOCAL_SIZE_X, LOCAL_SIZE_Y), NULL, NULL);
    clAssert(err);
}

void CLImageTransformProgram::phaseAngleBufferToImage(rev::CLTexture2D& img,
        rev::CLBuffer& buffer, int interBuffWidth, int interBuffHeight) {
    cl_int err;
    err = phaseAngleBufferToImgKernel.setArg(KernelBufferToImage::ARG_IMAGE,
            img.getBuffer());
    clAssert(err);

    err = phaseAngleBufferToImgKernel.setArg(KernelBufferToImage::ARG_IMG_WIDTH,
            img.getWidth());
    clAssert(err);

    err = phaseAngleBufferToImgKernel.setArg(KernelBufferToImage::ARG_IMG_HEIGHT,
            img.getHeight());
    clAssert(err);

    err = phaseAngleBufferToImgKernel.setArg(KernelBufferToImage::ARG_BUFFER,
            buffer.getBuffer());
    clAssert(err);

    err = phaseAngleBufferToImgKernel.setArg(KernelBufferToImage::ARG_BUFFER_WIDTH,
            interBuffWidth * 2);
    clAssert(err);

    cl::CommandQueue& queue = CLContext::get().commandQueue();
    err = queue.enqueueNDRangeKernel(phaseAngleBufferToImgKernel, cl::NullRange,
            cl::NDRange(interBuffWidth, interBuffHeight),
            cl::NDRange(LOCAL_SIZE_X, LOCAL_SIZE_Y), NULL, NULL);
    clAssert(err);
}

void CLImageTransformProgram::realPartBufferToImage(rev::CLTexture2D& img,
        rev::CLBuffer& buffer, int interBuffWidth, int interBuffHeight) {
    cl_int err;
    err = realPartBufferToImgKernel.setArg(KernelBufferToImage::ARG_IMAGE,
            img.getBuffer());
    clAssert(err);

    err = realPartBufferToImgKernel.setArg(KernelBufferToImage::ARG_IMG_WIDTH,
            img.getWidth());
    clAssert(err);

    err = realPartBufferToImgKernel.setArg(KernelBufferToImage::ARG_IMG_HEIGHT,
            img.getHeight());
    clAssert(err);

    err = realPartBufferToImgKernel.setArg(KernelBufferToImage::ARG_BUFFER,
            buffer.getBuffer());
    clAssert(err);

    err = realPartBufferToImgKernel.setArg(KernelBufferToImage::ARG_BUFFER_WIDTH,
            interBuffWidth * 2);
    clAssert(err);

    cl::CommandQueue& queue = CLContext::get().commandQueue();
    err = queue.enqueueNDRangeKernel(realPartBufferToImgKernel, cl::NullRange,
            cl::NDRange(interBuffWidth, interBuffHeight),
            cl::NDRange(LOCAL_SIZE_X, LOCAL_SIZE_Y), NULL, NULL);
    clAssert(err);
}

void CLImageTransformProgram::imagPartBufferToImage(rev::CLTexture2D& img,
        rev::CLBuffer& buffer, int interBuffWidth, int interBuffHeight) {
    cl_int err;
    err = imagPartBufferToImgKernel.setArg(KernelBufferToImage::ARG_IMAGE,
            img.getBuffer());
    clAssert(err);

    err = imagPartBufferToImgKernel.setArg(KernelBufferToImage::ARG_IMG_WIDTH,
            img.getWidth());
    clAssert(err);

    err = imagPartBufferToImgKernel.setArg(KernelBufferToImage::ARG_IMG_HEIGHT,
            img.getHeight());
    clAssert(err);

    err = imagPartBufferToImgKernel.setArg(KernelBufferToImage::ARG_BUFFER,
            buffer.getBuffer());
    clAssert(err);

    err = imagPartBufferToImgKernel.setArg(KernelBufferToImage::ARG_BUFFER_WIDTH,
            interBuffWidth * 2);
    clAssert(err);

    cl::CommandQueue& queue = CLContext::get().commandQueue();
    err = queue.enqueueNDRangeKernel(imagPartBufferToImgKernel, cl::NullRange,
            cl::NDRange(interBuffWidth, interBuffHeight),
            cl::NDRange(LOCAL_SIZE_X, LOCAL_SIZE_Y), NULL, NULL);
    clAssert(err);
}
