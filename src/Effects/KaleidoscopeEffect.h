/* 
 * File:   KaleidoscopeEffect.h
 * Author: Revers
 *
 * Created on 18 czerwiec 2012, 07:55
 */

#ifndef KALEIDOSCOPEEFFECT_H
#define	KALEIDOSCOPEEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class KaleidoscopeEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    KaleidoscopeEffect(ControlPanel* controlPanel);

    virtual ~KaleidoscopeEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
};

#endif	/* KALEIDOSCOPEEFFECT_H */

