/*
 * FFT2DLensBlurEffect.cpp
 *
 *  Created on: 25-09-2012
 *      Author: Revers
 */

#include "../ControlPanel.h"

#include <cmath>
#include <GL/glew.h>
#include <GL/gl.h>

#include "FFT2DLensBlurEffect.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <rev/common/RevAssert.h>
#include <rev/common/RevProfiler.h>
#include <rev/common/RevCommonUtil.h>
#include <rev/cl/RevCLAssert.h>


using namespace rev;
using namespace log4cplus;
using namespace std;

#define EFFECT_GROUP "FFT2D Lens Blur"
#define EFFECT_PREFIX "fftLens_"
#define KERNEL_FILE "cl_files/FFT2DLensBlur.cl"

#define DEFAULT_RADIUS 10.0f
#define DEFAULT_BLOOM 1.76f
#define DEFAULT_BLOOM_THRESHOLD 0.88f
#define DEFAULT_ANGLE 0.0f
#define DEFAULT_SIDES 5

#define COMPONENT_COUNT 2
#define COMPONENT_SIZE (sizeof(cl_float) * COMPONENT_COUNT)

//#ifdef clAssert
//#undef clAssert
//#endif
//#define clAssert(x) ((void)0)

namespace KernelCopyTileToBuffers {

    enum {
        ARG_IMAGE,
        ARG_RED_BUFFER,
        ARG_GREEN_BUFFER,
        ARG_BLUE_BUFFER,
        ARG_TILE_WIDTH,
        ARG_TILE_START_X,
        ARG_TILE_START_Y,
        ARG_IMAGE_WIDTH,
        ARG_IMAGE_HEIGHT,
        ARG_BLOOM_THRESHOLD,
        ARG_BLOOM
    };
}

namespace KernelMultiplyTiles {

    enum {
        ARG_RED_BUFFER,
        ARG_GREEN_BUFFER,
        ARG_BLUE_BUFFER,
        ARG_MASK_BUFFER,
        ARG_TILE_WIDTH,
        ARG_SCALE_FACTOR,
    };
}

namespace KernelCopyBuffersToTile {

    enum {
        ARG_IMAGE,
        ARG_RED_BUFFER,
        ARG_GREEN_BUFFER,
        ARG_BLUE_BUFFER,
        ARG_TILE_WIDTH,
        ARG_TILE_HEIGHT,
        ARG_TILE_START_X,
        ARG_TILE_START_Y,
        ARG_IMAGE_WIDTH,
        ARG_IMAGE_HEIGHT,
        ARG_IFILTER_RADIUS
    };
}

Logger FFT2DLensBlurEffect::logger = Logger::getInstance("effects.FFT2DLensBlur");

FFT2DLensBlurEffect::FFT2DLensBlurEffect(ControlPanel* controlPanel) :
AbstractEffect(controlPanel) {

    radius = DEFAULT_RADIUS;
    bloom = DEFAULT_BLOOM;
    bloomThreshold = DEFAULT_BLOOM_THRESHOLD;
    angle = DEFAULT_ANGLE;
    sides = DEFAULT_SIDES;
}

bool FFT2DLensBlurEffect::init() {
    LOG_TRACE(logger, "init()");

    if (!createProgram(KERNEL_FILE, clProgram)) {
        LOG_ERROR(logger, "Compilation of file " KERNEL_FILE " FAILED!!");
        return false;
    }

    if (!initKernels()) {
        LOG_ERROR(logger, "initKernels() FAILED!!");
        return false;
    }

    if (!initBuffers()) {
        LOG_ERROR(logger, "initBuffers() FAILED!!");
        return false;
    }

    reset();

    TwAddVarCB(effectBar, EFFECT_PREFIX "Sides", TW_TYPE_INT32, setSidesCallback,
            getSidesCallback, this, "label='Sides:' min=3 max=12 step=1 group='"
            EFFECT_GROUP "' ");

    TwAddVarCB(effectBar, EFFECT_PREFIX "Angle", TW_TYPE_FLOAT, setAngleCallback,
            getAngleCallback, this,
            "label='Angle:' min=0.0 max=6.29 step=0.01 group='"
            EFFECT_GROUP "' ");

    TwAddVarCB(effectBar, EFFECT_PREFIX "Radius", TW_TYPE_FLOAT,
            setRadiusCallback, getRadiusCallback, this,
            "label='Radius:' min=0.0 max=120.0 step=1.0 group='"
            EFFECT_GROUP "' ");

    TwAddVarCB(effectBar, EFFECT_PREFIX "Bloom", TW_TYPE_FLOAT, setBloomCallback,
            getBloomCallback, this,
            "label='Bloom:' min=1.0 max=3.0 step=0.01 group='"
            EFFECT_GROUP "' ");

    TwAddVarCB(effectBar, EFFECT_PREFIX "BloomThreshold", TW_TYPE_FLOAT,
            setBloomThresholdCallback, getBloomThresholdCallback, this,
            "label='Bloom Threshold:' min=0.0 max=1.0 step=0.01 group='"
            EFFECT_GROUP "' ");

    addLoadButton(EFFECT_PREFIX "effect", EFFECT_GROUP);

    int opened = 1;
    TwSetParam(effectBar, EFFECT_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

bool FFT2DLensBlurEffect::initKernels() {

    cl_int err;

    copyToBuffersKernel = cl::Kernel(clProgram, "copyTileToBuffers", &err);
    if (err != CL_SUCCESS) {
        clAssert(err);
        return false;
    }

    multiplyTilesKernel = cl::Kernel(clProgram, "multiplyTiles", &err);
    if (err != CL_SUCCESS) {
        clAssert(err);
        return false;
    }

    copyToImageKernel = cl::Kernel(clProgram, "copyBuffersToImage", &err);
    if (err != CL_SUCCESS) {
        clAssert(err);
        return false;
    }

    return true;
}

#define SIZE_IN_MB(x) ((float)(x) / (1024.0f * 1024.f))

bool FFT2DLensBlurEffect::initBuffers() {
    int imageWidth = controlPanel->inputTexture.getWidth();
    int imageHeight = controlPanel->inputTexture.getHeight();

    iFilterRadius = (int) ceil(radius);

    int width =
            iFilterRadius < 32 ?
            std::min(int(128), imageWidth + 2 * iFilterRadius) :
            std::min(int(256), imageWidth + 2 * iFilterRadius);
    int height =
            iFilterRadius < 32 ?
            std::min(int(128), imageHeight + 2 * iFilterRadius) :
            std::min(int(256), imageHeight + 2 * iFilterRadius);

    width = (int) pow(2.0, ceil(log2((double) width)));
    height = (int) pow(2.0, ceil(log2((double) height)));

    if (tileWidth == width && tileHeight == height) {
        return true;
    }

    tileWidth = width;
    tileHeight = height;

    //----------------------------------------------------------------------
    // INFO:
    int tileXOffset = tileWidth - 2 * iFilterRadius;
    int tileYOffset = tileHeight - 2 * iFilterRadius;
    int tileXCount = (imageWidth + iFilterRadius - 1) / tileXOffset;
    int tileYCount = (imageHeight + iFilterRadius - 1) / tileYOffset;
    cout << "Expected " << R(tileXCount) << endl << R(tileYCount)
            << endl << R(tileWidth)
            << endl << R(tileHeight)
            << endl << R(imageWidth)
            << endl << R(imageHeight)
            << endl << R(iFilterRadius)
            << endl << R(tileXOffset)
            << endl << R(tileYOffset) << endl;
    //=======================================================================

    //    if (!outputBuffer.create(controlPanel->inputTexture.getSize())) {
    //        assert(false);
    //        return false;
    //    }

    LOG_INFO(logger,
            "Creating new tile buffers " << tileWidth << "x" << tileHeight << " (for image " << imageWidth << "x" << imageHeight << ").");

    if (!redTileBuffer.create(tileWidth * tileHeight * COMPONENT_SIZE)) {
        assert(false);
        return false;
    }

    if (!greenTileBuffer.create(tileWidth * tileHeight * COMPONENT_SIZE)) {
        assert(false);
        return false;
    }

    if (!blueTileBuffer.create(tileWidth * tileHeight * COMPONENT_SIZE)) {
        assert(false);
        return false;
    }

    if (!polygonTileBuffer.create(tileWidth * tileHeight * COMPONENT_SIZE)) {
        assert(false);
        return false;
    }

    int totalSize = /*outputBuffer.getSize() + */redTileBuffer.getSize()
            + greenTileBuffer.getSize() + blueTileBuffer.getSize()
            + polygonTileBuffer.getSize();

    LOG_INFO(logger, "Total buffer size: " << CommonUtil::getSizeMB(totalSize));

    if (!initFFT2DAlgorithm()) {
        return false;
    }

    return true;
}

bool FFT2DLensBlurEffect::initFFT2DAlgorithm() {
    Profiler p;
    p.start();

    CLFFT2DInPlaceAbstractFactory& abstractFactory =
            CLFFT2DInPlaceAbstractFactory::getInstance();

    clFFT2DPtr = abstractFactory.create(
            ICLAlgorithm::OPTION_TYPE_FLOAT | ICLAlgorithm::OPTION_ARR_INTERLEAVED
            | ICLAlgorithm::OPTION_SCALED_FALSE);

    if (!clFFT2DPtr) {
        assert(false);
        return false;
    }

    if (clFFT2DPtr->init(tileWidth, tileHeight) == false) {
        assert(false);
        return false;
    }

    p.stop();
    LOG_INFO(logger,
            "Creation of FFT2D Algorithm took " << StringUtil::format("%.6f", p.getTotalTimeSec()) << " seconds.\n");

    if (!initMaskTile()) {
        return false;
    }

    return true;
}

/**
 * Return a mod b. This differs from the % operator with respect to negative numbers.
 * @param a the dividend
 * @param b the divisor
 * @return a mod b
 */
double my_mod(double a, double b) {
    int n = (int) (a / b);

    a -= n * b;
    if (a < 0)
        return a + b;
    return a;
}

bool FFT2DLensBlurEffect::initMaskTile() {

    LOG_INFO(logger,
            "Creating mask tile. " << rev::a2s(R(tileWidth), R(tileHeight), R(radius)));

    float* mask = new float[tileWidth * tileHeight * 2];

    // Create the kernel
    double polyAngle = M_PI / sides;
    double polyScale = 1.0f / cos(polyAngle);
    double r2 = radius * radius;
    double rangle = angle; //Math.toRadians(angle);
    float total = 0;
    int i = 0;
    for (int y = 0; y < tileHeight; y++) {
        for (int x = 0; x < tileWidth; x++) {
            double dx = x - tileWidth / 2.0;
            double dy = y - tileHeight / 2.0;
            double r = dx * dx + dy * dy;
            double f = r < r2 ? 1 : 0;
            if (f != 0) {
                r = sqrt(r);
                if (sides != 0) {
                    double a = atan2(dy, dx) + rangle;
                    a = my_mod(a, polyAngle * 2) - polyAngle;
                    f = cos(a) * polyScale;
                } else {
                    f = 1;
                }
                f = f * r < radius ? 1 : 0;
            }
            total += (float) f;

            mask[i * 2] = (float) f;
            mask[i * 2 + 1] = 0.0f;
            i++;
        }
    }

    // Normalize the kernel
    i = 0;
    float scaleFactor = 1.0f / total;
    for (int y = 0; y < tileHeight; y++) {
        for (int x = 0; x < tileWidth; x++) {
            mask[i * 2] *= scaleFactor;
            i++;
        }
    }

    if (!polygonTileBuffer.write(mask)) {
        assert(false);
        return false;
    }

    delete[] mask;

    Profiler p;
    clFFT2DPtr->fft(polygonTileBuffer, p);

    LOG_INFO(logger,
            "FFT2D for polygonTile done in " << StringUtil::format("%.6f", p.getTotalTimeSec()) << " seconds.\n");

    return true;
}

void FFT2DLensBlurEffect::runCopyToBuffersKernel(int tileStartX, int tileStartY) {
    cl_int err;
    err = copyToBuffersKernel.setArg(KernelCopyTileToBuffers::ARG_IMAGE,
            controlPanel->inputTexture.getBuffer());
    clAssert(err);

    err = copyToBuffersKernel.setArg(KernelCopyTileToBuffers::ARG_RED_BUFFER,
            redTileBuffer.getBuffer());
    clAssert(err);

    err = copyToBuffersKernel.setArg(KernelCopyTileToBuffers::ARG_GREEN_BUFFER,
            greenTileBuffer.getBuffer());
    clAssert(err);

    err = copyToBuffersKernel.setArg(KernelCopyTileToBuffers::ARG_BLUE_BUFFER,
            blueTileBuffer.getBuffer());
    clAssert(err);

    err = copyToBuffersKernel.setArg(KernelCopyTileToBuffers::ARG_TILE_START_X,
            tileStartX);
    clAssert(err);

    err = copyToBuffersKernel.setArg(KernelCopyTileToBuffers::ARG_TILE_START_Y,
            tileStartY);
    clAssert(err);

    err = copyToBuffersKernel.setArg(KernelCopyTileToBuffers::ARG_TILE_WIDTH,
            (int) tileWidth);
    clAssert(err);

    err = copyToBuffersKernel.setArg(KernelCopyTileToBuffers::ARG_IMAGE_WIDTH,
            (int) controlPanel->inputTexture.getWidth());
    clAssert(err);

    err = copyToBuffersKernel.setArg(KernelCopyTileToBuffers::ARG_IMAGE_HEIGHT,
            (int) controlPanel->inputTexture.getHeight());
    clAssert(err);

    err = copyToBuffersKernel.setArg(KernelCopyTileToBuffers::ARG_BLOOM_THRESHOLD,
            bloomThreshold);
    clAssert(err);

    err = copyToBuffersKernel.setArg(KernelCopyTileToBuffers::ARG_BLOOM,
            bloom);
    clAssert(err);

    cl::CommandQueue& queue = CLContext::get().commandQueue();
    err = queue.enqueueNDRangeKernel(copyToBuffersKernel, cl::NullRange,
            cl::NDRange(tileWidth, tileHeight), cl::NullRange, NULL, NULL);
    clAssert(err);
}

void FFT2DLensBlurEffect::runMultiplyTilesKernel(int tileStartX, int tileStartY) {
    cl_int err;

    err = multiplyTilesKernel.setArg(KernelMultiplyTiles::ARG_RED_BUFFER,
            redTileBuffer.getBuffer());
    clAssert(err);

    err = multiplyTilesKernel.setArg(KernelMultiplyTiles::ARG_GREEN_BUFFER,
            greenTileBuffer.getBuffer());
    clAssert(err);

    err = multiplyTilesKernel.setArg(KernelMultiplyTiles::ARG_BLUE_BUFFER,
            blueTileBuffer.getBuffer());
    clAssert(err);

    err = multiplyTilesKernel.setArg(KernelMultiplyTiles::ARG_MASK_BUFFER,
            polygonTileBuffer.getBuffer());
    clAssert(err);

    err = multiplyTilesKernel.setArg(KernelMultiplyTiles::ARG_TILE_WIDTH,
            (int) tileWidth);
    clAssert(err);

    float scaleFactor = 1.0f / float(tileWidth * tileHeight);

    err = multiplyTilesKernel.setArg(KernelMultiplyTiles::ARG_SCALE_FACTOR,
            scaleFactor);
    clAssert(err);

    cl::CommandQueue& queue = CLContext::get().commandQueue();
    err = queue.enqueueNDRangeKernel(multiplyTilesKernel, cl::NullRange,
            cl::NDRange(tileWidth, tileHeight), cl::NullRange, NULL, NULL);
    clAssert(err);
}

void FFT2DLensBlurEffect::runCopyToImageKernel(int tileStartX, int tileStartY) {
    cl_int err;
    err = copyToImageKernel.setArg(KernelCopyBuffersToTile::ARG_IMAGE,
            controlPanel->outputTexture.getBuffer());
    clAssert(err);

    err = copyToImageKernel.setArg(KernelCopyBuffersToTile::ARG_RED_BUFFER,
            redTileBuffer.getBuffer());
    clAssert(err);

    err = copyToImageKernel.setArg(KernelCopyBuffersToTile::ARG_GREEN_BUFFER,
            greenTileBuffer.getBuffer());
    clAssert(err);

    err = copyToImageKernel.setArg(KernelCopyBuffersToTile::ARG_BLUE_BUFFER,
            blueTileBuffer.getBuffer());
    clAssert(err);

    err = copyToImageKernel.setArg(KernelCopyBuffersToTile::ARG_TILE_START_X,
            tileStartX);
    clAssert(err);

    err = copyToImageKernel.setArg(KernelCopyBuffersToTile::ARG_TILE_START_Y,
            tileStartY);
    clAssert(err);

    err = copyToImageKernel.setArg(KernelCopyBuffersToTile::ARG_TILE_WIDTH,
            (int) tileWidth);
    clAssert(err);

    err = copyToImageKernel.setArg(KernelCopyBuffersToTile::ARG_TILE_HEIGHT,
            (int) tileHeight);
    clAssert(err);

    err = copyToImageKernel.setArg(KernelCopyBuffersToTile::ARG_IMAGE_WIDTH,
            (int) controlPanel->inputTexture.getWidth());
    clAssert(err);

    err = copyToImageKernel.setArg(KernelCopyBuffersToTile::ARG_IMAGE_HEIGHT,
            (int) controlPanel->inputTexture.getHeight());
    clAssert(err);

    err = copyToImageKernel.setArg(KernelCopyBuffersToTile::ARG_IFILTER_RADIUS,
            iFilterRadius);
    clAssert(err);

    int xPixels = tileWidth - 2 * iFilterRadius;
    int yPixels = tileHeight - 2 * iFilterRadius;

    cl::CommandQueue& queue = CLContext::get().commandQueue();
    err = queue.enqueueNDRangeKernel(copyToImageKernel, cl::NullRange,
            cl::NDRange(xPixels, yPixels), cl::NullRange, NULL, NULL);
    clAssert(err);
}

void FFT2DLensBlurEffect::transformOneTile(int tileStartX, int tileStartY) {
    cl::CommandQueue& queue = CLContext::get().commandQueue();
    runCopyToBuffersKernel(tileStartX, tileStartY);
    queue.finish();

    clFFT2DPtr->fft(redTileBuffer);
    queue.finish();
    clFFT2DPtr->fft(greenTileBuffer);
    queue.finish();
    clFFT2DPtr->fft(blueTileBuffer);
    queue.finish();
    runMultiplyTilesKernel(tileStartX, tileStartY);
    queue.finish();

    clFFT2DPtr->fft(redTileBuffer, true);
    queue.finish();
    clFFT2DPtr->fft(greenTileBuffer, true);
    queue.finish();
    clFFT2DPtr->fft(blueTileBuffer, true);
    queue.finish();

    runCopyToImageKernel(tileStartX, tileStartY);
    queue.finish();
}

void FFT2DLensBlurEffect::render() {

    int imageWidth = (int) controlPanel->inputTexture.getWidth();
    int imageHeight = (int) controlPanel->inputTexture.getHeight();

    int tileXOffset = (int) tileWidth - 2 * (int) iFilterRadius;
    int tileYOffset = (int) tileHeight - 2 * (int) iFilterRadius;

    for (int tileY = -(int) iFilterRadius; tileY < (int) imageHeight; tileY +=
            tileYOffset) {
        for (int tileX = -(int) iFilterRadius; tileX < (int) imageWidth; tileX +=
                tileXOffset) {

            transformOneTile(tileX, tileY);

        }
    }

}

void FFT2DLensBlurEffect::textureChanged(rev::CLTexture2D& texture) {
    if (!initBuffers()) {
        LOG_ERROR(logger, "initBuffers() FAILED!!");
    }
}

void FFT2DLensBlurEffect::reset() {
    radius = DEFAULT_RADIUS;
    bloom = DEFAULT_BLOOM;
    bloomThreshold = DEFAULT_BLOOM_THRESHOLD;
    angle = DEFAULT_ANGLE;
    sides = DEFAULT_SIDES;
}

const char* FFT2DLensBlurEffect::getName() {
    return EFFECT_GROUP;
}

void FFT2DLensBlurEffect::updateFilter() {
    if (!initBuffers()) {
        LOG_ERROR(logger, "initBuffers() FAILED!!");
    }
}

void FFT2DLensBlurEffect::updatePolygon() {
    if (!initMaskTile()) {
        LOG_ERROR(logger, "initMaskTile() FAILED!!");
    }
}

void TW_CALL
FFT2DLensBlurEffect::setRadiusCallback(const void* value, void* clientData) {
    FFT2DLensBlurEffect* effect = static_cast<FFT2DLensBlurEffect*> (clientData);
    if (!effect->amILoaded()) {
        return;
    }

    effect->radius = *(const float*) value;

    effect->updateFilter();
    effect->updatePolygon();
}

void TW_CALL
FFT2DLensBlurEffect::getRadiusCallback(void* value, void* clientData) {

    FFT2DLensBlurEffect* effect = static_cast<FFT2DLensBlurEffect*> (clientData);

    *(float*) value = effect->radius;
}

void TW_CALL
FFT2DLensBlurEffect::setBloomCallback(const void* value, void* clientData) {
    FFT2DLensBlurEffect* effect = static_cast<FFT2DLensBlurEffect*> (clientData);
    if (!effect->amILoaded()) {
        return;
    }

    effect->bloom = *(const float*) value;
}

void TW_CALL
FFT2DLensBlurEffect::getBloomCallback(void* value, void* clientData) {

    FFT2DLensBlurEffect* effect = static_cast<FFT2DLensBlurEffect*> (clientData);

    *(float*) value = effect->bloom;
}

void TW_CALL
FFT2DLensBlurEffect::setBloomThresholdCallback(const void* value,
        void* clientData) {
    FFT2DLensBlurEffect* effect = static_cast<FFT2DLensBlurEffect*> (clientData);
    if (!effect->amILoaded()) {
        return;
    }

    effect->bloomThreshold = *(const float*) value;
}

void TW_CALL
FFT2DLensBlurEffect::getBloomThresholdCallback(void* value, void* clientData) {

    FFT2DLensBlurEffect* effect = static_cast<FFT2DLensBlurEffect*> (clientData);

    *(float*) value = effect->bloomThreshold;
}

void TW_CALL
FFT2DLensBlurEffect::setAngleCallback(const void* value, void* clientData) {
    FFT2DLensBlurEffect* effect = static_cast<FFT2DLensBlurEffect*> (clientData);
    if (!effect->amILoaded()) {
        return;
    }

    effect->angle = *(const float*) value;
    effect->updatePolygon();
}

void TW_CALL
FFT2DLensBlurEffect::getAngleCallback(void* value, void* clientData) {

    FFT2DLensBlurEffect* effect = static_cast<FFT2DLensBlurEffect*> (clientData);

    *(float*) value = effect->angle;
}

void TW_CALL
FFT2DLensBlurEffect::setSidesCallback(const void* value, void* clientData) {
    FFT2DLensBlurEffect* effect = static_cast<FFT2DLensBlurEffect*> (clientData);
    if (!effect->amILoaded()) {
        return;
    }

    effect->sides = *(const int*) value;
    effect->updatePolygon();
}

void TW_CALL
FFT2DLensBlurEffect::getSidesCallback(void* value, void* clientData) {
    FFT2DLensBlurEffect* effect = static_cast<FFT2DLensBlurEffect*> (clientData);

    *(int*) value = effect->sides;
}
