/*
 * FFT2D.cpp
 *
 *  Created on: 21-09-2012
 *      Author: Revers
 */

#include "FFT2D.h"
#include "../LoggingDefines.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <rev/common/RevAssert.h>
#include <rev/common/RevProfiler.h>
#include <rev/cl/RevCLAssert.h>

using namespace std;
using namespace rev;
using namespace log4cplus;

Logger FFT2D::logger = Logger::getInstance("effects.FFT2D.fft2D");

bool FFT2D::init(int width, int height) {
    this->width = width;
    this->height = height;

    if (!transposeProgram.init()) {
        LOG_ERROR(logger, "transposeProgram.init() FAILED!!");
        return false;
    }

    if (!createFFTAlgorithms()) {
        LOG_ERROR(logger, "createFFTAlgorithms() FAILED!!");
        assert(false);
        return false;
    }

    return true;
}

bool FFT2D::reinit(int width, int height) {
    this->width = width;
    this->height = height;

    if (!createFFTAlgorithms()) {
        LOG_ERROR(logger, "createFFTAlgorithms() FAILED!!");

        assert(false);
        return false;
    }

    return true;
}

void FFT2D::fft(rev::CLBuffer& colorBuffer, rev::CLBuffer& interBuffer,
        float scaleFactor) {

    int w = width;
    int h = height;

    int origin = 0;
    int size = w * COMPONENT_SIZE;

    cl_int err;
    for (int i = 0; i < h; i++) {
        clFFTHorizontalPtr->setBufferOffset(origin);
        clFFTHorizontalPtr->fft(colorBuffer);
        origin += size;
    }

    float sf = 1.0f / w * scaleFactor;
    transposeProgram.transposeMulNorm(colorBuffer, interBuffer, w, h, sf);
    colorBuffer.swap(interBuffer);

    w = height;
    h = width;

    origin = 0;
    size = w * COMPONENT_SIZE;

    for (int i = 0; i < h; i++) {
        clFFTVerticalPtr->setBufferOffset(origin);
        clFFTVerticalPtr->fft(colorBuffer);
        origin += size;
    }

    sf = 1.0f / w * scaleFactor;
    transposeProgram.transposeMulNorm(colorBuffer, interBuffer, w, h, sf);
    colorBuffer.swap(interBuffer);
}

bool FFT2D::createFFTAlgorithms() {
    alwaysAssertMsg(width <= 2048 && height <= 2048,
            "This FFT algorithm handles only data below or equal 2048 units. " << rev::a2s(R(width), R(height)));

    CLFFTInPlaceAbstractFactory& abstractFactory =
            CLFFTInPlaceAbstractFactory::getInstance();

    clFFTHorizontalPtr = abstractFactory.create(
            ICLAlgorithm::OPTION_TYPE_FLOAT | ICLAlgorithm::OPTION_ARR_INTERLEAVED);

    if (!clFFTHorizontalPtr) {
        assert(false);
        return false;
    }

    if (clFFTHorizontalPtr->init(width) == false) {
        assert(false);
        return false;
    }

    clFFTVerticalPtr = abstractFactory.create(
            ICLAlgorithm::OPTION_TYPE_FLOAT | ICLAlgorithm::OPTION_ARR_INTERLEAVED);

    if (!clFFTVerticalPtr) {
        assert(false);
        return false;
    }

    if (clFFTVerticalPtr->init(height) == false) {
        assert(false);
        return false;
    }

    return true;
}
