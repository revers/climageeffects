/*
 * CLImageTransformProgram.h
 *
 *  Created on: 20-09-2012
 *      Author: Revers
 */

#ifndef CLIMAGETRANSFORMPROGRAM_H_
#define CLIMAGETRANSFORMPROGRAM_H_

#include "../CLImageEffectsConfig.h"

#include <rev/cl/RevCLProgram.h>
#include <rev/cl/RevCLBuffer.h>
#include <rev/cl/RevCLTexture2D.h>

namespace log4cplus {
    class Logger;
}

namespace rev {

    class CLImageTransformProgram {
        static log4cplus::Logger logger;

        CLProgram program;
        cl::Kernel colorToBufferKernel;
        cl::Kernel modBufferToImgKernel;
        cl::Kernel phaseAngleBufferToImgKernel;
        cl::Kernel realPartBufferToImgKernel;
        cl::Kernel imagPartBufferToImgKernel;
    public:
        CLImageTransformProgram() {
        }
        virtual ~CLImageTransformProgram() {
        }

        bool init();

        void copyImageColorToBuffer(rev::CLTexture2D& img, rev::CLBuffer& buffer,
                int interBuffWidth, int interBuffHeight);

        void modBufferToImage(rev::CLTexture2D& img, rev::CLBuffer& buffer,
                int interBuffWidth, int interBuffHeight);

        void phaseAngleBufferToImage(rev::CLTexture2D& img, rev::CLBuffer& buffer,
                int interBuffWidth, int interBuffHeight);

        void realPartBufferToImage(rev::CLTexture2D& img, rev::CLBuffer& buffer,
                int interBuffWidth, int interBuffHeight);

        void imagPartBufferToImage(rev::CLTexture2D& img, rev::CLBuffer& buffer,
                int interBuffWidth, int interBuffHeight);

    private:
        int roundUp(int group_size, int global_size) {
            if (group_size == 0)
                return 0;
            int r = global_size % group_size;
            if (r == 0) {
                return global_size;
            } else {
                return global_size + group_size - r;
            }
        }
    };

}
/* namespace rev */
#endif /* CLIMAGETRANSFORMPROGRAM_H_ */
