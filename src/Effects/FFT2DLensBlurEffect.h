/*
 * FFT2DLensBlurEffect.h
 *
 *  Created on: 25-09-2012
 *      Author: Revers
 */

#ifndef FFT2DLENSBLUREFFECT_H_
#define FFT2DLENSBLUREFFECT_H_

#include "AbstractEffect.h"

#include <rev/cl/RevCLBuffer.h>

#include <rev/cla/api/RevICLFFT2DInPlace.h>
#include <rev/cla/api/RevCLFFT2DInPlaceAbstractFactory.h>

namespace log4cplus {
    class Logger;
}
;

class FFT2DLensBlurEffect: public AbstractEffect {
    static log4cplus::Logger logger;
    float radius;
    float bloom;
    float bloomThreshold;
    float angle;
    int sides;

    int xTileCount = 0;
    int yTileCount = 0;
    int tileWidth = 0;
    int tileHeight = 0;
    int iFilterRadius = 0;

    rev::CLBuffer redTileBuffer;
    rev::CLBuffer greenTileBuffer;
    rev::CLBuffer blueTileBuffer;
    rev::CLBuffer polygonTileBuffer;

    cl::Kernel copyToBuffersKernel;
    cl::Kernel multiplyTilesKernel;
    cl::Kernel copyToImageKernel;

    rev::CLFFT2DInPlacePtr clFFT2DPtr;

public:
    FFT2DLensBlurEffect(ControlPanel* controlPanel);

    virtual ~FFT2DLensBlurEffect() {
    }

    /**
     * @Override
     */
    bool init();

    /**
     * @Override
     */
    void reset();

    /**
     * @Override
     */
    const char* getName();

    /**
     * @Override
     */
    void textureChanged(rev::CLTexture2D& texture);

    /**
     * @Override
     */
    void render();

    /**
     * @Override
     */
    //    void resize(int width, int height) {
    //    }
private:
    bool initKernels();
    bool initBuffers();
    bool initMaskTile();
    bool initFFT2DAlgorithm();

    void transformOneTile(int tileStartX, int tileStartY);

    void runCopyToBuffersKernel(int tileStartX, int tileStartY);
    void runMultiplyTilesKernel(int tileStartX, int tileStartY);
    void runCopyToImageKernel(int tileStartX, int tileStartY);

    void updateFilter();
    void updatePolygon();

    static void TW_CALL setRadiusCallback(const void* value,
            void* clientData);
    static void TW_CALL getRadiusCallback(void* value, void* clientData);

    static void TW_CALL setBloomCallback(const void* value, void* clientData);
    static void TW_CALL getBloomCallback(void* value, void* clientData);

    static void TW_CALL setBloomThresholdCallback(const void* value,
            void* clientData);
    static void TW_CALL getBloomThresholdCallback(void* value, void* clientData);

    static void TW_CALL setAngleCallback(const void* value, void* clientData);
    static void TW_CALL getAngleCallback(void* value, void* clientData);

    static void TW_CALL setSidesCallback(const void* value, void* clientData);
    static void TW_CALL getSidesCallback(void* value, void* clientData);
};

#endif /* FFT2DLENSBLUREFFECT_H_ */
